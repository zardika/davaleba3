package com.example.davaleba3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class Seconddavaleba : AppCompatActivity() {

    private lateinit var ageEditText: EditText
    private lateinit var nextButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seconddavaleba)

        ageEditText = findViewById(R.id.editTextAge)
        nextButton = findViewById(R.id.buttonSecondNext)

        var name = ""

        if (intent.extras != null){

            name = intent.extras?.getString("NAME" , "").toString()
        }

        nextButton.setOnClickListener{

            val age =   ageEditText.text.toString().toInt()

            val intent = Intent(this, TrewiActivity::class.java)
            intent.putExtra("NAME" , name)
            intent.putExtra("AGE" , age)
            startActivity(intent)
            finish()
        }

    }
}