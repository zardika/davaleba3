package com.example.davaleba3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    private lateinit var nameEditText: EditText
    private lateinit var nextButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d("MyData", "OnCreate")
        nameEditText = findViewById(R.id.editTextName)
        nextButton = findViewById(R.id.buttonNext)

        nextButton.setOnClickListener{
            val name = nameEditText.text.toString()

            val intent = Intent(this , Seconddavaleba::class.java)
            intent.putExtra("NAME" , name)
            startActivity(intent)
            finish()
        }
    }


    override fun onStart() {
        super.onStart()

        Log.d("MyData", "OnStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d("MyData", "OnResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d("MyData", "OnPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d("MyData", "OnStop")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("MyData", "OnRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("MyData", "OnDestroy")
    }
}