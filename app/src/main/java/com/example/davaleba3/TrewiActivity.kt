package com.example.davaleba3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class TrewiActivity : AppCompatActivity() {

    private lateinit var phoneEditText: EditText
    private lateinit var nextButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trewi)

        phoneEditText = findViewById(R.id.editTextPhone)
        nextButton = findViewById(R.id.buttonFinnish)

        var name = ""

        if (intent.extras != null){

            name = intent.extras?.getString("NAME" , "").toString()
        }

        var age = 0

        if (intent.extras != null){
            age = intent.extras?.getInt("AGE", 0)!!
        }

        nextButton.setOnClickListener{
            val phone = phoneEditText.text.toString().toInt()

            val intent = Intent(this , FinnishActivity::class.java)
            intent.putExtra("PHONE" , phone)
            intent.putExtra("AGE" , age)
            intent.putExtra("NAME", name)
            startActivity(intent)
            finish()
        }

    }
}