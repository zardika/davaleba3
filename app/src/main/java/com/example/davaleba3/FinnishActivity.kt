package com.example.davaleba3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class FinnishActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finnish)

        findViewById<TextView>(R.id.textView1).text =
            intent.extras?.getString("NAME" , "Zardo")
        findViewById<TextView>(R.id.textView2).text =
            intent.getIntExtra("AGE", 0).toString()
        findViewById<TextView>(R.id.textView3).text =
            intent.extras?.getInt("PHONE", 0).toString()
    }
}